import React, { Component } from 'react';

class Player extends Component {
  constructor(props) {
    super(props);
    this.state =  { 
      editMode: false,
      firstname: props.player.firstname,
      lastname: props.player.lastname
    };

    this.handleFirstnameChange = this.handleFirstnameChange.bind(this);
    this.handleLastnameChange = this.handleLastnameChange.bind(this);
    this.handleSaveSubmit = this.handleSaveSubmit.bind(this);
    this.handleEditSubmit = this.handleEditSubmit.bind(this);
    this.handleDeleteSubmit = this.handleDeleteSubmit.bind(this);
  }

  componentDidMount() {
    // Nothing to mount.
  }

  componentWillUnmount() {
    // Nothing to unmount.
  }

  handleFirstnameChange(event) {
    this.setState({firstname: event.target.value});
  }

  handleLastnameChange(event) {
    this.setState({lastname: event.target.value});
  }

  handleEditSubmit(event) {
    this.setState({ editMode: true });
    event.preventDefault();
  }

  handleSaveSubmit(event) {
    this.setState({ editMode: false });
    this.props.onPlayerSaveFormSubmit({
      id: this.props.player.id,
      firstname: this.state.firstname,
      lastname: this.state.lastname
    });
    event.preventDefault();
  }
  
  handleDeleteSubmit(event) {
    this.props.onPlayerDeleteFormSubmit(this.props.player);
    event.preventDefault();
  }

  render() {
    const playerNonEditable = (
      <div className="Player" id={this.props.player.id}>
        <div className="Firstname">{this.props.player.firstname}</div>
        <div className="Lastname">{this.props.player.lastname}</div>
        <form className="PlayerEdit" onSubmit={this.handleEditSubmit}>
          <input type="submit" value="Edit" />
        </form>
        <form className="PlayerDelete" onSubmit={this.handleDeleteSubmit}>
          <input type="submit" value="Delete" />
        </form>
      </div>
    );

    const playerEditable = (
        <div className="Player" id={this.props.player.id}>
          <input ref={(input) => { this.firstNameInput = input; }} type="text" value={this.state.firstname} onChange={this.handleFirstnameChange} />
          <input type="text" value={this.state.lastname} onChange={this.handleLastnameChange} />
          <form className="PlayerSave" onSubmit={this.handleSaveSubmit}>
            <input type="submit" value="Save" />
          </form>
          <form className="PlayerDelete" onSubmit={this.handleDeleteSubmit}>
            <input type="submit" value="Delete" />
          </form>
        </div>
      );

    if (this.state.editMode) {
      return playerEditable;
    } else {
      return playerNonEditable;
    }
  }
}

export default Player;