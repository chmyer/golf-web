import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Players from './Players';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to golf-web</h1>
        </header>
        <div>
          <Players />
        </div>
      </div>
    );
  }
}

export default App;
