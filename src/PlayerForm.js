import React, { Component } from 'react';

class PlayerForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      lastname: ''
    };

    this.handleFirstnameChange = this.handleFirstnameChange.bind(this);
    this.handleLastnameChange = this.handleLastnameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.firstNameInput.focus();
  }

  componentWillUnmount() {
    // Nothing to unmount.
  }

  handleFirstnameChange(event) {
    this.setState({firstname: event.target.value});
  }

  handleLastnameChange(event) {
    this.setState({lastname: event.target.value});
  }

  handleSubmit(event) {
    this.props.onPlayerFormSubmit({
      firstname: this.state.firstname,
      lastname: this.state.lastname
    });
    this.setState({
      firstname: '',
      lastname: ''
    });
    this.firstNameInput.focus();
    event.preventDefault();
  }

  render() {
    return (
      <form className="PlayerForm" onSubmit={this.handleSubmit}>
        <input ref={(input) => { this.firstNameInput = input; }} type="text" value={this.state.firstname} onChange={this.handleFirstnameChange} />
        <input type="text" value={this.state.lastname} onChange={this.handleLastnameChange} />
        <input type="submit" value="Add" />
      </form>
    );
  }
}

export default PlayerForm;