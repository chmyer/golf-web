import React, { Component } from 'react';
import './Players.css';
import Player from './Player';
import PlayerForm from './PlayerForm';

class Players extends Component {
  constructor(props) {
    super(props);
    this.state = {
      players: []
    };

    this.handlePlayerFormSubmit = this.handlePlayerFormSubmit.bind(this);
    this.handlePlayerSaveFormSubmit = this.handlePlayerSaveFormSubmit.bind(this);
    this.handlePlayerDeleteSubmit = this.handlePlayerDeleteSubmit.bind(this);
  }

  componentDidMount() {
    fetch('http://localhost:5050/player')
      .then(result => result.json())
      .then(
        (result) => {
          this.setState({ players: result });
        }
      )
  }

  componentWillUnmount() {
    // Nothing to unmount.
  }

  handlePlayerFormSubmit(player) {
    fetch('http://localhost:5050/player', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(player)
    }).then(res => res.json())
      .then(
        (result) => {
          var players = this.state.players.slice();
          players.push(result);
          this.setState({ players: players });
        }
      )
  }

  handlePlayerSaveFormSubmit(player) {
    fetch('http://localhost:5050/player/' + player.id, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT',
      body: JSON.stringify(player)
    }).then(res => res.json())
      .then(
        (result) => {
          var players = this.state.players.slice();
          for (var i = 0; i < players.length; i++) {
            if (players[i].id === result.id) {
              players.splice(i, 1, result);
              this.setState({ players: players });
              break;
            }
          }
        }
      )
  }

  handlePlayerDeleteSubmit(player) {
    fetch('http://localhost:5050/player/' + player.id, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'DELETE'
    }).then(
      (result) => {
        var players = this.state.players.slice();
        players.splice(players.indexOf(player), 1);
        this.setState({ players: players });
      }
    )
  }

  render() {
    const content = this.state.players.map((player) =>
      <Player key={player.id} player={player} onPlayerSaveFormSubmit={this.handlePlayerSaveFormSubmit} onPlayerDeleteFormSubmit={this.handlePlayerDeleteSubmit}/>
    );

    return (
      <div>
        <div className="Players">
          {content}
        </div>
        <PlayerForm onPlayerFormSubmit={this.handlePlayerFormSubmit}/>
      </div>
    );
  }
}

export default Players;
